#!/usr/bin/env bash

if [ ! -f .env.local ]; then
  cp .env .env.local
fi

case $1 in
  'composer')
    cmd="exec php $*";
    ;;
  'sf')
    shift;
    cmd="exec php bin/console $*";
    ;;
  'dbmigrate')
    shift;
    cmd="exec php bin/console doctrine:migrations:migrate -n $*"
    ;;
  'fixtures')
    shift;
    cmd="exec php bin/console doctrine:fixtures:load --append $*"
    ;;
  'proxy')
    shift;
    docker rm -f nginx-reverse-proxy;
    docker network create --driver bridge reverse-proxy;
    docker pull jwilder/nginx-proxy;
    docker run -d \
      -p80:80 \
      --name nginx-reverse-proxy \
      --net reverse-proxy \
      -v /var/run/docker.sock:/tmp/docker.sock:ro \
      -e LISTENED_PORT=80 \
      --restart always \
      jwilder/nginx-proxy;
      exit;
    ;;
  *)
    cmd="$*";
    ;;
esac

docker compose --env-file .env.local ${cmd}
