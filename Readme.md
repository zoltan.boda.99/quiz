First steps after cloning the repo:

```shell
# starting up containers (the first run of the helper
# creates .env.local from .env file)
./dc.sh up -d
# starting up nginx reverse proxy
./dc.sh proxy
# running composer install
./dc.sh composer install
# creating app db, if needed
./dc.sh sf doctrine:database:create
# running db migrations
./dc.sh dbmigrate
# loading fixtures with append
./dc.sh fixtures

# running any other docker-compose command as needed
./dc.sh ps
./dc.sh exec php bash
./dc.sh sf doctrine:fixtures:load
```
*[dc.sh](./dc.sh) is a helper script for running `docker-compose` commands.

** if port 80 is taken on the host the specific service should be reconfigured or stopped temporarily 

After the containers start up the project will be accessable via the URL [http://quiz.loc](http://quiz.loc) as defined in `PROJECT_ACCESS` env
var found in the [.env.local](.env.local) file. To make it work a line needs to be added in the `hosts` file:
```text
127.0.0.1 quiz.loc
```

A [PHPMyAdmin](http://quiz.loc/pma) is also available under `/pma` URL in case it comes in handy.

Admin account details:
- username: `admin`
- password: `admin`