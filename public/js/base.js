$( document ).ready(function() {
    $(".answer .form-check input").on('click', function () {
        let questionId = ($(this).attr('data-question-id'));
        let mode = $(this).attr('data-mode');
        let answer = $(this).val();
        let paramstring = mode + '/' + questionId + '/' + answer;

        $.ajax({
            url: "/user/answer/check/" + paramstring,
            success: function (result) {
                if (result.message) {
                    $("#answer_" + questionId + "_message")
                        .html(result.message)
                        .addClass(
                            result.success ? 'text-success' : 'text-danger'
                        );
                }

                $(".answer-question-" + questionId)
                    .css('pointer-events', 'none')
                    .css('opacity', '.2');
            },
        });
    });

    var countDownInSec = BaseSettings.countdownInSec ? BaseSettings.countdownInSec : 60 *5;
    var deadline = new Date(Date.now() + 1000 * countDownInSec).getTime();
    var x = setInterval(function() {
        var now = new Date().getTime();
        var t = deadline - now;

        // var days = Math.floor(t / (1000 * 60 * 60 * 24));
        // var hours = Math.floor((t%(1000 * 60 * 60 * 24))/(1000 * 60 * 60));
        var minutes = Math.floor((t % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((t % (1000 * 60)) / 1000);

        document.getElementById("countdown").innerHTML = String(minutes).padStart(2, '0') + ":" + String(seconds).padStart(2, '0');
        if (t < 0) {
            clearInterval(x);
            document.getElementById("countdown").innerHTML = "00:00";
            if (!$("#answer_firstName").val()) {
                $("#answer_firstName").val('Unknown');
            }
            if (!$("#answer_lastName").val()) {
                $("#answer_lastName").val('Unknown');
            }
            var validRegex = /^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/;
            var email = $("#answer_email").val();
            if (
                       !email
                    || !email.match(validRegex)
            ) {
                $("#answer_email").val('unknown@unknown.biz');
            }
            $('form[name="answer"]').submit();
        }
    }, 1000);

});
