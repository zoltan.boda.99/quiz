<?php

namespace App\Service;

use App\Entity\QuestionBinary;
use App\Entity\QuestionMultiple;
use App\Model\AnswerCheckResponseModel;
use App\Repository\QuestionBinaryRepository;
use App\Repository\QuestionMultipleRepository;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;

class AnswerService
{
	public function __construct(
		private readonly QuestionMultipleRepository $questionMultipleRepository,
		private readonly QuestionBinaryRepository $questionBinaryRepository,
	)
	{}

	private function getRepository(string $mode = 'binary'): ServiceEntityRepository
	{
		if ('binary' === $mode) {
			return $this->questionBinaryRepository;
		} else {
			return $this->questionMultipleRepository;
		}
	}

	public function checkAnswer($mode, $id, $answer): array
	{
		$repository = $this->getRepository($mode);
		/** @var QuestionBinary|QuestionMultiple $question */
		$question = $repository->find($id);
		$success = ((int) $question->getRightAnswer() === (int) $answer);
		$message = $success ? 'Correct! ' : 'Sorry, you are wrong! ';
		$message .= 'The right answer is: "' . $question->getRightAnswerText() . '".';

		return [
			'success' => $success,
			'message' => $message,
		];
	}

	/**
	 * @param QuestionBinary[]|QuestionMultiple[] $questions
	 * @param array|null $answers
	 * @return AnswerCheckResponseModel
	 */
	public function checkAnswers(array $questions, ?array $answers = null): AnswerCheckResponseModel
	{
		$unanswered = isset($answers) ? count($questions) - count($answers) : count($questions);
		$score = 0;
		if ($unanswered < count($questions)) {
			foreach ($questions as $question) {
				$id = $question->getId();
				if (
						   isset($answers[$id])
						&& (int) $question->getRightAnswer() === (int) $answers[$id]
				) {
					$score++;
				}
			}
		}

		return new AnswerCheckResponseModel(
			score: $score,
			unanswered: $unanswered
		);
	}
}