<?php

namespace App\Entity;

use App\Repository\AnswerRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: AnswerRepository::class)]
class Answer
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 50)]
	#[Assert\NotBlank]
    private string $firstName;

    #[ORM\Column(length: 50)]
	#[Assert\NotBlank]
    private string $lastName;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
	#[Assert\Email]
    private string $email;

    #[ORM\Column]
    private ?int $totalScore = null;

    #[ORM\Column]
    private ?int $totalUnanswered = null;

    #[ORM\Column]
    private ?\DateTimeImmutable $submittedAt = null;

    #[ORM\Column]
    private ?int $timeTaken = null;

	private ?array $answers = null;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    public function setFirstName(string $firstName): self
    {
        $this->firstName = $firstName;

        return $this;
    }

    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    public function setLastName(string $lastName): self
    {
        $this->lastName = $lastName;

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = $email;

        return $this;
    }

    public function getTotalScore(): ?int
    {
        return $this->totalScore;
    }

    public function setTotalScore(int $totalScore): self
    {
        $this->totalScore = $totalScore;

        return $this;
    }

    public function getTotalUnanswered(): ?int
    {
        return $this->totalUnanswered;
    }

    public function setTotalUnanswered(int $totalUnanswered): self
    {
        $this->totalUnanswered = $totalUnanswered;

        return $this;
    }

    public function getSubmittedAt(): ?\DateTimeImmutable
    {
        return $this->submittedAt;
    }

    public function setSubmittedAt(\DateTimeImmutable $submittedAt): self
    {
        $this->submittedAt = $submittedAt;

        return $this;
    }

    public function getTimeTaken(): ?int
    {
        return $this->timeTaken;
    }

    public function setTimeTaken(int $timeTaken): self
    {
        $this->timeTaken = $timeTaken;

        return $this;
    }

	/**
	 * @return array|null
	 */
	public function getAnswers(): ?array
	{
		return $this->answers;
	}

	/**
	 * @param array|null $answers
	 */
	public function setAnswers(?array $answers): void
	{
		$this->answers = $answers;
	}
}
