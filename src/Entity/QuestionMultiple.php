<?php

namespace App\Entity;

use App\Repository\QuestionMultipleRepository;
use Doctrine\DBAL\Types\Types;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: QuestionMultipleRepository::class)]
class QuestionMultiple
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
    private string $text;

    #[ORM\Column(type: Types::SMALLINT)]
	#[Assert\NotBlank]
    private int $rightAnswer;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
    private string $answer1;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
    private string $answer2;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
    private string $answer3;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRightAnswer(): ?int
    {
        return $this->rightAnswer;
    }

	public function getRightAnswerText(): string
	{
		$propertyName = 'answer' . $this->rightAnswer;

		return $this->{$propertyName};
	}

	public function setRightAnswer(int $rightAnswer): self
    {
        $this->rightAnswer = $rightAnswer;

        return $this;
    }

    public function getAnswer1(): ?string
    {
        return $this->answer1;
    }

    public function setAnswer1(string $answer1): self
    {
        $this->answer1 = $answer1;

        return $this;
    }

    public function getAnswer2(): ?string
    {
        return $this->answer2;
    }

    public function setAnswer2(string $answer2): self
    {
        $this->answer2 = $answer2;

        return $this;
    }

    public function getAnswer3(): ?string
    {
        return $this->answer3;
    }

    public function setAnswer3(string $answer3): self
    {
        $this->answer3 = $answer3;

        return $this;
    }
}
