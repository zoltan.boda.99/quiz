<?php

namespace App\Entity;

use App\Repository\QuestionBinaryRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

#[ORM\Entity(repositoryClass: QuestionBinaryRepository::class)]
class QuestionBinary
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column]
    private ?int $id = null;

    #[ORM\Column(length: 255)]
	#[Assert\NotBlank]
    private string $text;

    #[ORM\Column]
    private bool $rightAnswer;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getText(): ?string
    {
        return $this->text;
    }

    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    public function getRightAnswer(): bool
    {
        return $this->rightAnswer;
    }

	public function getRightAnswerText(): string
	{
		return $this->rightAnswer ? 'Yes' : 'No';
	}

	public function setRightAnswer(bool $rightAnswer): self
    {
        $this->rightAnswer = $rightAnswer;

        return $this;
    }
}
