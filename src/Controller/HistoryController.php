<?php

namespace App\Controller;

use App\Repository\AnswerRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/history')]
#[IsGranted('ROLE_ADMIN')]
class HistoryController extends AbstractController
{
	public function __construct(
		private readonly AnswerRepository $answerRepository,
	)
	{}

	#[Route('/', name: 'app_history_index', methods: ['GET'])]
    public function index(): Response
    {
		$answers = $this->answerRepository->findBy([], ['submittedAt' => 'desc']);
        return $this->render('answer/history.html.twig', [
            'answers' => $answers,
        ]);
    }
}
