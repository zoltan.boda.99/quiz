<?php

namespace App\Controller;

use App\Entity\Answer;
use App\Form\AnswerType;
use App\Repository\AnswerRepository;
use App\Repository\QuestionBinaryRepository;
use App\Repository\QuestionMultipleRepository;
use App\Service\AnswerService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/answer')]
class AnswerController extends AbstractController
{
	public function __construct(
		private readonly QuestionBinaryRepository $questionBinaryRepository,
		private readonly QuestionMultipleRepository $questionMultipleRepository,
		private readonly AnswerRepository $answerRepository,
		private readonly AnswerService $answerService,
	)
	{}

	#[Route('/', name: 'app_answer_index', methods: ['GET'])]
    public function index(): Response
    {
		$answers = $this->answerRepository->findBy([], ['totalScore' => 'desc', 'timeTaken' => 'asc']);
        return $this->render('answer/index.html.twig', [
            'answers' => $answers,
        ]);
    }

    #[Route('/new', name: 'app_answer_new', methods: ['GET', 'POST'])]
    public function new(Request $request): Response
    {
		$mode = $request->getSession()->get('quizMode', 'binary');
		if ('binary' === $mode) {
			$questions = $this->questionBinaryRepository->findAll();
		} else {
			$questions = $this->questionMultipleRepository->findAll();
		}

        $answer = new Answer();
        $form = $this->createForm(AnswerType::class, $answer);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
			$result = $this->answerService->checkAnswers($questions, $request->get('answers'));
			$startedAt = \DateTime::createFromFormat('Y-m-d H:i:s', $request->get('started_at'));
			$submittedAt = new \DateTimeImmutable();

			$answer
				->setTotalScore($result->getScore())
				->setTotalUnanswered($result->getUnanswered())
				->setSubmittedAt($submittedAt)
				->setTimeTaken($submittedAt->getTimestamp() - $startedAt->getTimestamp())
			;
            $this->answerRepository->save($answer, true);

            return $this->redirectToRoute('app_answer_statistics', [
				'id' => $answer->getId()
			], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('answer/new.html.twig', [
            'answer' => $answer,
            'form' => $form,
			'mode' => $mode,
            'questions' => $questions,
        ]);
    }

	#[Route('/statistics/{id}', name: 'app_answer_statistics', requirements: ['id' => '\d+'], methods: ['GET', 'POST'])]
	public function statistics(Answer $answer): Response
	{
		return $this->render('answer/statistics.html.twig', [
			'answer' => $answer,
		]);
	}

	#[Route('/check/{mode}/{id}/{answer}', name: 'app_answer_check', requirements: ['id' => '\d+', 'answer' => '\d+'], defaults: ['mode' => 'binary'], methods: ['GET', 'POST'])]
	public function checkAnswer($mode, $id, $answer): Response
	{
		$result = $this->answerService->checkAnswer($mode, $id, $answer);

		return $this->json($result, Response::HTTP_OK);
	}
}
