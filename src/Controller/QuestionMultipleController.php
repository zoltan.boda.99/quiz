<?php

namespace App\Controller;

use App\Entity\QuestionMultiple;
use App\Form\QuestionMultipleType;
use App\Repository\QuestionMultipleRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/question/multiple')]
#[IsGranted('ROLE_ADMIN')]
class QuestionMultipleController extends AbstractController
{
    #[Route('/', name: 'app_question_multiple_index', methods: ['GET'])]
    public function index(QuestionMultipleRepository $questionMultipleRepository): Response
    {
        return $this->render('question_multiple/index.html.twig', [
            'question_multiples' => $questionMultipleRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_question_multiple_new', methods: ['GET', 'POST'])]
    public function new(Request $request, QuestionMultipleRepository $questionMultipleRepository): Response
    {
        $questionMultiple = new QuestionMultiple();
        $form = $this->createForm(QuestionMultipleType::class, $questionMultiple);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $questionMultipleRepository->save($questionMultiple, true);

            return $this->redirectToRoute('app_question_multiple_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('question_multiple/new.html.twig', [
            'question_multiple' => $questionMultiple,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_question_multiple_show', methods: ['GET'])]
    public function show(QuestionMultiple $questionMultiple): Response
    {
        return $this->render('question_multiple/show.html.twig', [
            'question_multiple' => $questionMultiple,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_question_multiple_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, QuestionMultiple $questionMultiple, QuestionMultipleRepository $questionMultipleRepository): Response
    {
        $form = $this->createForm(QuestionMultipleType::class, $questionMultiple);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $questionMultipleRepository->save($questionMultiple, true);

            return $this->redirectToRoute('app_question_multiple_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('question_multiple/edit.html.twig', [
            'question_multiple' => $questionMultiple,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_question_multiple_delete', methods: ['POST'])]
    public function delete(Request $request, QuestionMultiple $questionMultiple, QuestionMultipleRepository $questionMultipleRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$questionMultiple->getId(), $request->request->get('_token'))) {
            $questionMultipleRepository->remove($questionMultiple, true);
        }

        return $this->redirectToRoute('app_question_multiple_index', [], Response::HTTP_SEE_OTHER);
    }
}
