<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/user/settings')]
class SettingsController extends AbstractController
{
	private const DEFAULT_MODE = 'binary';
	private const ALLOWED_MODES = ['binary', 'multiple'];

	#[Route('/{mode}', name: 'app_settings_index', methods: ['GET'])]
    public function index(Request $request, ?string $mode = null): Response
    {
		if (empty($mode)) {
			$mode = $request->getSession()->get('quizMode', 'binary');
		} else {
			$newMode = (in_array($mode, self::ALLOWED_MODES))
				? $mode
				: self::DEFAULT_MODE
			;
			$request->getSession()->set('quizMode', $newMode);
		}

        return $this->render('settings/index.html.twig', [
            'currentMode' => $mode,
			'availableModes' => self::ALLOWED_MODES
        ]);
    }
}
