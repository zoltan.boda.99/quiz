<?php

namespace App\Controller;

use App\Entity\QuestionBinary;
use App\Form\QuestionBinaryType;
use App\Repository\QuestionBinaryRepository;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\IsGranted;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/admin/question/binary')]
#[IsGranted('ROLE_ADMIN')]
class QuestionBinaryController extends AbstractController
{
    #[Route('/', name: 'app_question_binary_index', methods: ['GET'])]
    public function index(QuestionBinaryRepository $questionBinaryRepository): Response
    {
        return $this->render('question_binary/index.html.twig', [
            'question_binaries' => $questionBinaryRepository->findAll(),
        ]);
    }

    #[Route('/new', name: 'app_question_binary_new', methods: ['GET', 'POST'])]
    public function new(Request $request, QuestionBinaryRepository $questionBinaryRepository): Response
    {
        $questionBinary = new QuestionBinary();
        $form = $this->createForm(QuestionBinaryType::class, $questionBinary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $questionBinaryRepository->save($questionBinary, true);

            return $this->redirectToRoute('app_question_binary_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('question_binary/new.html.twig', [
            'question_binary' => $questionBinary,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_question_binary_show', methods: ['GET'])]
    public function show(QuestionBinary $questionBinary): Response
    {
        return $this->render('question_binary/show.html.twig', [
            'question_binary' => $questionBinary,
        ]);
    }

    #[Route('/{id}/edit', name: 'app_question_binary_edit', methods: ['GET', 'POST'])]
    public function edit(Request $request, QuestionBinary $questionBinary, QuestionBinaryRepository $questionBinaryRepository): Response
    {
        $form = $this->createForm(QuestionBinaryType::class, $questionBinary);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $questionBinaryRepository->save($questionBinary, true);

            return $this->redirectToRoute('app_question_binary_index', [], Response::HTTP_SEE_OTHER);
        }

        return $this->renderForm('question_binary/edit.html.twig', [
            'question_binary' => $questionBinary,
            'form' => $form,
        ]);
    }

    #[Route('/{id}', name: 'app_question_binary_delete', methods: ['POST'])]
    public function delete(Request $request, QuestionBinary $questionBinary, QuestionBinaryRepository $questionBinaryRepository): Response
    {
        if ($this->isCsrfTokenValid('delete'.$questionBinary->getId(), $request->request->get('_token'))) {
            $questionBinaryRepository->remove($questionBinary, true);
        }

        return $this->redirectToRoute('app_question_binary_index', [], Response::HTTP_SEE_OTHER);
    }
}
