<?php

namespace App\Model;

class AnswerCheckResponseModel
{
	public function __construct(
		private readonly int $score,
		private readonly int $unanswered
	)
	{}

	/**
	 * @return int
	 */
	public function getScore(): int
	{
		return $this->score;
	}

	/**
	 * @return int
	 */
	public function getUnanswered(): int
	{
		return $this->unanswered;
	}
}