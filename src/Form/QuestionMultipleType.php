<?php

namespace App\Form;

use App\Entity\QuestionMultiple;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class QuestionMultipleType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('text')
            ->add('rightAnswer', ChoiceType::class, [
				'choices' => [1 => 1, 2 => 2, 3 => 3],
				'expanded' => true,
			])
            ->add('answer1')
            ->add('answer2')
            ->add('answer3')
        ;
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => QuestionMultiple::class,
        ]);
    }
}
