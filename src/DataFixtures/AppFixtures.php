<?php

namespace App\DataFixtures;

use App\Entity\QuestionBinary;
use App\Entity\QuestionMultiple;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
		$binary = (new QuestionBinary())
			->setText('First Binary')
			->setRightAnswer(true)
		;
		$manager->persist($binary);

		$binary = (new QuestionBinary())
			->setText('Second Binary')
			->setRightAnswer(false)
		;
		$manager->persist($binary);

		$binary = (new QuestionBinary())
			->setText('Third Binary')
			->setRightAnswer(true)
		;
		$manager->persist($binary);

		$multiple = (new QuestionMultiple())
			->setText('First Multiple')
			->setRightAnswer(1)
			->setAnswer1('First Answer')
			->setAnswer2('Second Answer')
			->setAnswer3('Third Answer')
		;
		$manager->persist($multiple);

		$multiple = (new QuestionMultiple())
			->setText('Second Multiple')
			->setRightAnswer(2)
			->setAnswer1('First Answer')
			->setAnswer2('Second Answer')
			->setAnswer3('Third Answer')
		;
		$manager->persist($multiple);

		$multiple = (new QuestionMultiple())
			->setText('Third Multiple')
			->setRightAnswer(3)
			->setAnswer1('First Answer')
			->setAnswer2('Second Answer')
			->setAnswer3('Third Answer')
		;
		$manager->persist($multiple);

		$manager->flush();
    }
}
